package sample;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Sample {

    // http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html

    private ScriptEngineManager manager;
    private ScriptEngine engine;

    @Before
    public void setUp() {
        manager = new ScriptEngineManager();
        engine = manager.getEngineByName("nashorn");
    }

    @Test
    public void test() throws ScriptException {
        assertThat(engine.eval("1 + 2"), is(3));

        engine.eval("var x = {a: 1, b: 2, c: 3}");
        assertThat(engine.eval("x.a"), is(1));

        // double?
        assertThat(engine.eval("[1, 4, 3, 3].reduce(function (r, n) { return r + n; })"), is(11.0));
    }

    public interface Accumulator {
        public int add(int x);
    }

    @Test
    public void test2() throws ScriptException {
        engine.eval("function add(n){ this.sum = this.sum ? this.sum + n : n; return this.sum; }");

        Invocable invocable = (Invocable) engine;
        Accumulator accum = invocable.getInterface(Accumulator.class);
        assertThat(accum.add(1), is(1));
        assertThat(accum.add(3), is(4));
        assertThat(accum.add(2), is(6));
    }

    @Test(expected = ScriptException.class)
    public void test3() throws ScriptException {
        // ES6 not compatible
        engine.eval("const x = p => p % 2 == 0");
    }

    @Test
    public void test4() throws ScriptException {
        // Spell to enable import
        engine.eval("load('nashorn:mozilla_compat.js')");
        engine.eval("importClass(java.nio.file.Paths)");
        Path path = (Path) engine.eval("Paths.get('C:/Windows')");

        assertThat(path, is(Paths.get("C:\\Windows")));
    }

    @Test
    public void test5() throws ScriptException {
        engine.eval("var ArrayList = java.util.ArrayList");
        engine.eval("var list = new ArrayList();");
        engine.eval("list.add('a'); list.add(1); list.add({ obj: 'object'});");

        assertThat(engine.eval("list.get(1)"), is(1));
    }

    @Test
    public void test6() throws ScriptException {
        //noinspection unchecked
        Comparator<String> comp = (Comparator<String>)
                engine.eval("new java.util.Comparator({" +
                            "   compare: function(o1, o2) {" +
                            "       return o1.length() - o2.length();" +
                            "   }" +
                            "})");

        List<String> list = Lists.newArrayList("aaa", "12345", "z");
        Collections.sort(list, comp);

        assertThat(list.get(0), is("z"));
        assertThat(list.get(1), is("aaa"));
        assertThat(list.get(2), is("12345"));
    }

}
